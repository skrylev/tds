// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Interactable.h"
#include "Teleport.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ATeleport : public AInteractable
{
	GENERATED_BODY()

	ATeleport();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	AActor* ActorDestination;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float Cooldown;

	virtual void BeginPlay() override;

	virtual void Interact(AActor* OtherActor) override;

	/*
	 * if needs to disable collisionstarting timer to enable collision again after cooldown
	 */
	void FlipCollision();
	
private:
	FTimerHandle CoolDownTimer;

};
