// Fill out your copyright notice in the Description page of Project Settings.


#include "AccelerationArea.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "TDS/TDSCharacter.h"
#include "../Buffs/BuffActor.h"
#include "../CharacterBuffInterface.h"
#include "Engine/World.h"
#include "../Buffs/AccelerationBuff.h"
#include "TimerManager.h"
#include "Kismet/KismetSystemLibrary.h"

AAccelerationArea::AAccelerationArea() : AreaAcceleration(1.5), CooldownArea(2), bIsActive(true)
{
}

void AAccelerationArea::Interact(AActor* Actor)
{
    if (!bIsActive)
        return;
    auto Character = Cast<ICharacterBuffInterface>(Actor);
    if (UKismetSystemLibrary::DoesImplementInterface(Actor, UCharacterBuffInterface::StaticClass()))
        Character->ApplyAcceleration(AreaAcceleration);    
}

void AAccelerationArea::EndInteract(AActor* Actor)
{
    if (!bIsActive)
        return;
    auto Character = Cast<ICharacterBuffInterface>(Actor);
    if (UKismetSystemLibrary::DoesImplementInterface(Actor, UCharacterBuffInterface::StaticClass()))
        Character->ApplyAcceleration(1/AreaAcceleration);
    
    auto NewBuff = Cast<ABuffActor>(GetWorld()->SpawnActor(Buff));
    if (!IsValid(NewBuff))
        return;

    GetWorldTimerManager().SetTimer(Timer,
                                this,
                                &AAccelerationArea::SetActive,
                                2,
                                false);
    auto AccelerationBuff = Cast<AAccelerationBuff>(NewBuff);
    if (IsValid(AccelerationBuff))
        AccelerationBuff->SetBuffAcceleration(AreaAcceleration);
    Cast<ICharacterBuffInterface>(Actor)->AddBuff(NewBuff, 2);

    bIsActive = false;
}

void AAccelerationArea::SetActive()
{
    bIsActive = true;
}
