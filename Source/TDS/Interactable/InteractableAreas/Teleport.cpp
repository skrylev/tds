// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleport.h"

#include "TimerManager.h"

ATeleport::ATeleport() : Cooldown(5)
{
}

void ATeleport::BeginPlay()
{
    AInteractable::BeginPlay();
    check(Cooldown > 0)
}


void ATeleport::Interact(AActor* OtherActor)
{
    UE_LOG(LogTemp, Warning, TEXT("In Teleport %s\n-------------"), *GetName());

    if (!IsValid(OtherActor) || !IsValid(ActorDestination) || this == OtherActor)
        return;


    auto TeleportDestination = Cast<ATeleport>(ActorDestination);
    if (IsValid(TeleportDestination))
        TeleportDestination->FlipCollision();

    FVector Origin, Box;
    ActorDestination->GetActorBounds(true, Origin, Box);
    const FVector DestinationVector = ActorDestination->GetActorLocation() + FVector(0, 0, Box.Z);

    bool ok = OtherActor->TeleportTo(DestinationVector, OtherActor->GetActorRotation());
    this->FlipCollision();
}

void ATeleport::FlipCollision()
{
    if (GetWorld()->GetTimerManager().IsTimerPending(CoolDownTimer))
        return;
    if (GetActorEnableCollision())
    {
        GetWorld()->GetTimerManager().SetTimer(CoolDownTimer,
                                           this,
                                           &ATeleport::FlipCollision,
                                           Cooldown,
                                           false);
    }
    SetActorEnableCollision(!GetActorEnableCollision());
}
