// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Interactable.h"
#include "AccelerationArea.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AAccelerationArea : public AInteractable
{
	GENERATED_BODY()
	AAccelerationArea();

protected:
	virtual void Interact(AActor* OtherActor) override;
	virtual void EndInteract(AActor* OtherActor) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AreaAcceleration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CooldownArea;

	bool bIsActive;

	void SetActive();
	FTimerHandle Timer;

};
