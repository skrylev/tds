// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

class ABuffActor;
class UBoxComponent;

UCLASS()
class TDS_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
	void HandleBeginOverlapTrigger(UPrimitiveComponent* OverlappedComponent,
			                       AActor* OtherActor,
			                       UPrimitiveComponent* OtherComp,
			                       int32 OtherBodyIndex,
			                       bool bFromSweep,
			                       const FHitResult& SweepResult);

	UFUNCTION()
	void HandleEndOverlapTrigger(class UPrimitiveComponent* OverlappedComp,
								 class AActor* OtherActor,
								 class UPrimitiveComponent* OtherComp,
								 int32 OtherBodyIndex);
	
    virtual void Interact(AActor* OtherActor);
	virtual void EndInteract(AActor* OtherActor);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* DefaultMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    UBoxComponent* TriggerZone;

	//TODO think about change to defaults only
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AfterEffects, meta = (AllowPrivateAccess = "true"))
    TSubclassOf<ABuffActor> Buff;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = AfterEffects, meta = (AllowPrivateAccess = "true"))
	float BuffDuration;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
