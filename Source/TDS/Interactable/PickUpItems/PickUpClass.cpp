// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpClass.h"

#include "Kismet/KismetSystemLibrary.h"
#include "../CharacterBuffInterface.h"
#include "../Buffs/BuffActor.h"
#include "Engine/World.h"


APickUpClass::APickUpClass() : RotationRate(60,0,0)
{
}

void APickUpClass::Tick(float DeltaTime)
{
    AddActorLocalRotation(RotationRate * DeltaTime);
}

void APickUpClass::Interact(AActor* Actor)
{
    if (UKismetSystemLibrary::DoesImplementInterface(Actor, UCharacterBuffInterface::StaticClass())){
        Cast<ICharacterBuffInterface>(Actor)->AddBuff(Cast<ABuffActor>(GetWorld()->SpawnActor(Buff)));
        Destroy();
    }
}
