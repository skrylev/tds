// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemSpawner.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AItemSpawner::AItemSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetRootComponent(CreateDefaultSubobject<USceneComponent>("RootComponent"));
	
	DefaultRoot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	DefaultRoot->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AItemSpawner::BeginPlay()
{
	Super::BeginPlay();
	SpawnItem();
}

void AItemSpawner::SpawnItem()
{
	FVector Origin, Box;
	GetActorBounds(true, Origin, Box);
	
	const FVector NewLocation = Origin + FVector(0,0, 100);
	auto NewActor = GetWorld()->SpawnActor<APickUpClass>(ItemForSpawn,
																	NewLocation,
															FRotator(0,0,0));
	if(!IsValid(NewActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("AItemSpawner::Spawn failed"));
		return;
	}
	NewActor->OnDestroyed.AddDynamic(this, &AItemSpawner::HandleItemDestroyed);
}

void AItemSpawner::HandleItemDestroyed(AActor* Actor)
{
	GetWorldTimerManager().SetTimer(Timer, this, &AItemSpawner::SpawnItem, 2, false);
}

// Called every frame
void AItemSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

