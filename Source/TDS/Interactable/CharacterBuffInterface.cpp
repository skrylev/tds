// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBuffInterface.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Buffs/BuffActor.h"

ICharacterBuffInterface::ICharacterBuffInterface() : MaxSpeed(2000), MinSpeed(0), SpeedWithoutBorders(-100)
{
}

bool ICharacterBuffInterface::AddBuff(ABuffActor* Buff, float Rate)
{
    if (Buffs.Contains(Buff) || !IsValid(Buff))
    {
        UE_LOG(LogTemp, Warning, TEXT("Bad buff was rejected"));
        return false;
    }
    Buff->Buff(this, Rate);
    Buffs.Add(Buff);

    return true;
}

void ICharacterBuffInterface::ApplyAcceleration(const float Acceleration)
{
    if (Acceleration == 0)
        return;
    auto AsCharacter = Cast<ACharacter>(this);
    if (SpeedWithoutBorders < AsCharacter->GetCharacterMovement()->MaxWalkSpeed)
        SpeedWithoutBorders = AsCharacter->GetCharacterMovement()->MaxWalkSpeed;
    SpeedWithoutBorders *= Acceleration;
    AsCharacter->GetCharacterMovement()->MaxWalkSpeed = FMath::Clamp<float>(SpeedWithoutBorders, MinSpeed, MaxSpeed);
    UE_LOG(LogTemp, Warning, TEXT("Accel = %f, Speed = %f, WithoutBord = %i"),
                         Acceleration,
                         AsCharacter->GetCharacterMovement()->MaxWalkSpeed,
                         SpeedWithoutBorders);
}

bool ICharacterBuffInterface::RemoveBuff(ABuffActor* Buff)
{
    if (Buffs.Contains(Buff))
    {
        Buffs.Remove(Buff);
        return true;
    }
    return false;
}