// Fill out your copyright notice in the Description page of Project Settings.


#include "AccelerationBuff.h"

AAccelerationBuff::AAccelerationBuff() : ABuffActor(), BuffAcceleration(1.6)
{
}

float AAccelerationBuff::GetBuffAcceleration() const
{
    return BuffAcceleration;
}

void AAccelerationBuff::SetBuffAcceleration(float Acceleration)
{
    UE_LOG(LogTemp, Warning, TEXT("NewAccel = %f"), Acceleration);

    if (Acceleration <= 0)
        return;
    this->BuffAcceleration = Acceleration;
}

void AAccelerationBuff::Buff(ICharacterBuffInterface* Character, float Rate)
{
    ABuffActor::Buff(Character, Rate);
    BuffCharacter->ApplyAcceleration(BuffAcceleration);
}


void AAccelerationBuff::UnBuff()
{
    BuffCharacter->ApplyAcceleration(1/BuffAcceleration);
    ABuffActor::UnBuff();
    
}