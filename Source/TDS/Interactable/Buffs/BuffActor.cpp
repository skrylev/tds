// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffActor.h"
#include "TimerManager.h"

// Sets default values
ABuffActor::ABuffActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABuffActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABuffActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABuffActor::Buff(ICharacterBuffInterface* Character, float Rate)
{
	BuffCharacter = Character;
	TimerRate = Rate;
	GetWorldTimerManager().SetTimer(Timer,
									this,
									&ABuffActor::UnBuff,
									Rate,
									false);
}

void ABuffActor::UnBuff()
{
	BuffCharacter->RemoveBuff(this);
	this->Destroy();
}

