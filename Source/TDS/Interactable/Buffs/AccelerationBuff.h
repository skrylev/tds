// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffActor.h"
#include "AccelerationBuff.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AAccelerationBuff : public ABuffActor
{
	GENERATED_BODY()
	
	virtual void Buff(ICharacterBuffInterface* Character, float Rate = 2.f) override;
	virtual void UnBuff() override;

public:
	AAccelerationBuff();

	UFUNCTION(BlueprintGetter)
	virtual float GetBuffAcceleration() const;
	
	UFUNCTION(BlueprintSetter)
	virtual void SetBuffAcceleration(float BuffAcceleration);
	
protected:
	UPROPERTY(EditDefaultsOnly,
				BlueprintReadWrite,
				Category = AfterEffects,
				BlueprintSetter=SetBuffAcceleration,
				BlueprintGetter=GetBuffAcceleration,
				meta = (AllowPrivateAccess = "true"))
	float BuffAcceleration;
public:

};
