// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../CharacterBuffInterface.h"
#include "BuffActor.generated.h"

UCLASS()
class TDS_API ABuffActor : public AActor
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle Timer;
	ICharacterBuffInterface* BuffCharacter;

public:
	ABuffActor();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Buff(ICharacterBuffInterface* Character, float Rate = 2.f);
	virtual void UnBuff();

	float TimerRate; //set in Buff(ICharacterBuffInterface* Character, float Rate)
};
