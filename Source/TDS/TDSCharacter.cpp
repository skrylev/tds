// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"


ATDSCharacter::ATDSCharacter() : CharacterSpeed(700), WalkingAcceleration(0.3f), Acceleration(0)
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* Input)
{
	Super::SetupPlayerInputComponent(InputComponent);
	Input->BindAxis("MoveForward", this, &ATDSCharacter::MoveForward);
	Input->BindAxis("MoveRight", this, &ATDSCharacter::MoveRight);
	Input->BindAction("Walking", IE_Pressed, this, &ATDSCharacter::ActivateWalkMode);
	Input->BindAction("Walking", IE_Released, this, &ATDSCharacter::DeactivateWalkMode);
	Input->BindAction("Jumping", IE_Pressed, this, &ATDSCharacter::Jump);
	Input->BindAction("Jumping", IE_Released, this, &ATDSCharacter::StopJumping);
	GetCharacterMovement()->JumpZVelocity = 1000;
	GetCharacterMovement()->AirControl = 1;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed;
}

void ATDSCharacter::MoveForward(float AxisValue)
{
	if (AxisValue == 0.0f|| !IsValid(Controller))
		return;
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis((EAxis::X));
	AddMovementInput(Direction, AxisValue);
}

void ATDSCharacter::MoveRight(float AxisValue)
{
	if (AxisValue == 0.0f || !IsValid(Controller))
		return;
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(Rotation.Pitch, 0, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis((EAxis::Y));
	AddMovementInput(Direction, AxisValue);
}

void ATDSCharacter::ActivateWalkMode()
{
	// GetCharacterMovement()->MaxWalkSpeed = FMath::Clamp<float>(GetCharacterMovement()->MaxWalkSpeed * WalkingAcceleration,
	// 															MinSpeed,
	// 															MaxSpeed);
}

void ATDSCharacter::DeactivateWalkMode()
{
	// GetCharacterMovement()->MaxWalkSpeed = FMath::Clamp<float>(GetCharacterMovement()->MaxWalkSpeed / WalkingAcceleration,
	//                                                             MinSpeed,
	//                                                             MaxSpeed);
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			/*Rotate character to mouse*/
			FVector Location, Direction;
			PC->DeprojectMousePositionToWorld(Location, Direction);

			FVector RotationLook = FMath::LinePlaneIntersection(Location,
																Location + Direction,
																GetActorLocation(),
																FVector(0,0,1));
			
			FRotator RotatorToMouse = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), RotationLook);
			// RotatorToMouse.Pitch = 0;
			SetActorRotation(RotatorToMouse);
			
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}
